package be.kriscoolen.opdracht11;
import java.util.*;
public class WalletApp {
    public static void main(String[] args) {
        //Map<Coin,Integer> wallet = new HashMap<>();
        Map<Coin,Integer> wallet = new LinkedHashMap<>();
        wallet.put(Coin.ONE_CENT,3); //3*0.01=0.03
        wallet.put(Coin.TWO_CENT,4);//4*0.02=0.08
        wallet.put(Coin.FIVE_CENT,5);//5*0.05=0.25
        wallet.put(Coin.TEN_CENT,6);//6*0.1=0.6
        wallet.put(Coin.TWENTY_CENT,7);//7*0.2=1.4
        wallet.put(Coin.FIFTY_CENT,8);//8*0.5=4
        wallet.put(Coin.ONE_EURO,9);//9*1=9
        wallet.put(Coin.TWO_EURO,10);//10*2=20
        //totaal in wallet: 35,36 euro
        wallet.forEach((coin,number)-> System.out.println(number + " piece(s) of " + coin));
        System.out.println("----------------------------");
        System.out.printf("Total amount in wallet: €%.2f\n",getWalletContent(wallet));
    }
    public static double getWalletContent(Map<Coin,Integer> w){
        double total = 0;
        for(Coin c:w.keySet()){
            total+=w.get(c)*c.getValue();
        }
        return total/100;
    }
}
