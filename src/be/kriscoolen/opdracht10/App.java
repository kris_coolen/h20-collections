package be.kriscoolen.opdracht10;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        List<Person> pList= new ArrayList<>();
        Person p1 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);
        Person p2 = new Person("Peter","Parker",Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse",Gender.F,80,51,1.62);
        Person p4 = new Person("Maggy","Simpson",Gender.F,1,5,0.81);
        Person p5 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);
        Person p6 = new Person("Vera","Paumen",Gender.F,34,70,1.54);
        Person p7 = new Person("Anno","Niempje",Gender.F,20,54,1.56);
        pList.add(p1);pList.add(p2);pList.add(p3);pList.add(p4);pList.add(p5);pList.add(p6);pList.add(p7);

        List<Person> filteredAndSortedList = pList.stream().filter(p->p.getGender()==Gender.F)
                                                           .filter(p->p.getAge()>20)
                                                           .sorted(Comparator.comparingInt(Person::getAge))
                                                           .collect(Collectors.toList());
        filteredAndSortedList.forEach(System.out::println);
    }
}
