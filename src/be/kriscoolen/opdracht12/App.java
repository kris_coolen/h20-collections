package be.kriscoolen.opdracht12;

import java.util.*;

public class App {
    public static void main(String[] args) {
        SortedMap<String,Person> treeMap= new TreeMap<>();
        Person p1 = new Person("Bruce","Wayne", Gender.M,42,82,1.85);
        Person p2 = new Person("Peter","Parker", Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse", Gender.F,80,51,1.62);
        Person p4 = new Person("Maggy","Simpson", Gender.F,1,5,0.81);
        Person p5 = new Person("Bruce","Wayne", Gender.M,42,82,1.85);
        Person p6 = new Person("Vera","Paumen", Gender.F,34,70,1.54);
        Person p7 = new Person("Anno","Niempje", Gender.F,20,54,1.56);
        treeMap.put(p1.getLastName()+" "+p1.getFirstName(),p1);
        treeMap.put(p2.getLastName()+" "+p2.getFirstName(),p2);
        treeMap.put(p3.getLastName()+" "+p3.getFirstName(),p3);
        treeMap.put(p4.getLastName()+" "+p4.getFirstName(),p4);
        treeMap.put(p5.getLastName()+" "+p5.getFirstName(),p5);
        treeMap.put(p6.getLastName()+" "+p6.getFirstName(),p6);
        treeMap.put(p7.getLastName()+" "+p7.getFirstName(),p7);

        treeMap.forEach((name,person)-> System.out.println("key: " + name + "\nvalue:"+person));

        System.out.println("Retrieve value from key ('Paumen Vera')");
        Person Vera = treeMap.get("Paumen Vera");
        System.out.println(Vera);

        System.out.println("Eerste persoon:");
        System.out.println(treeMap.get(treeMap.firstKey()));

        System.out.println("Laatste persoon:");
        System.out.println(treeMap.get(treeMap.lastKey()));


    }
}
