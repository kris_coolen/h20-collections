package be.kriscoolen.opdracht4;

import java.util.NavigableSet;
import java.util.Scanner;
import java.util.TreeSet;

public class TreeSetApp {
    static Scanner keyboard = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Geef een aantal woorden gescheiden door een spatie:");
        String[] words = keyboard.nextLine().split(" ");
        NavigableSet<String> tree = new TreeSet<>();
        for(String w: words) tree.add(w);
        System.out.println("Dit zijn de woorden die je hebt ingegeven:");
        tree.forEach(System.out::println);
        System.out.println("Het alfabetisch eerste woord: " + tree.first());
        System.out.println("Het alfabetisch laatste woord: " + tree.last());

    }
}
