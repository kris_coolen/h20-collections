package be.kriscoolen.opdracht3;

import java.util.*;

public class LotteryApp {
    static Scanner keyboard= new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Speel mee met de lotto.");
        System.out.println("Geef 6 verschillende getallen (één voor één)");
        Set<Integer> userNumbers = new LinkedHashSet<>();
        while(userNumbers.size()<6){
            try{
                Integer gok = keyboard.nextInt();
                if(gok<1||gok>45){
                    System.out.println("Geef een getal in tussen 1 en 45");
                }
                else{
                    userNumbers.add(gok);
                }
            }catch(InputMismatchException ime){
                System.out.println("Gelieve een getal in te geven!");
            }
            finally{
                keyboard.nextLine();
            }
        }
        Set<Integer> solutionSet = new HashSet<>();
        while(solutionSet.size()<6){
            solutionSet.add(new Random().nextInt(45)+1);
        }
        System.out.println("De winnende cijfers waren:");
        solutionSet.forEach(System.out::println);

        Set<Integer> matchingNumbers= new HashSet<>(solutionSet);
        matchingNumbers.retainAll(userNumbers);

        System.out.println("Je hebt " + matchingNumbers.size() + " juiste cijfers, namelijk: " );
        matchingNumbers.forEach(System.out::println);
    }
}
