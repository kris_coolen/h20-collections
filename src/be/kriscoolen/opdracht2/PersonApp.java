package be.kriscoolen.opdracht2;

import java.util.HashSet;
import java.util.Set;

public class PersonApp {
    public static void main(String[] args) {
        Set<Person> personSet = new HashSet<>();
        Person p1 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);
        Person p2 = new Person("Peter","Parker",Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse",Gender.F,26,51,1.62);
        Person p4 = new Person("Maggy","Simpson",Gender.F,1,5,0.81);
        Person p5 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);

        personSet.add(p1);
        personSet.add(p2);
        personSet.add(p3);
        personSet.add(p4);
        personSet.add(p5);

        System.out.println("Number of persons in set: " + personSet.size());
        personSet.forEach(System.out::println);

    }



}
