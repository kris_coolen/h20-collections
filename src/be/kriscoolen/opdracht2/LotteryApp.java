package be.kriscoolen.opdracht2;

import java.util.*;

public class LotteryApp {
    static Scanner keyboard= new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Speel mee met de lotto.");
        System.out.println("Geef 6 verschillende getallen (één voor één)");
        //user will play the lotto game and chooses 6 different numbers between 1 and 45
        Set<Integer> userNumbers = new HashSet<>();
        while(userNumbers.size()<6){
            try{
                Integer gok = keyboard.nextInt();
                if(gok<1||gok>45){
                    System.out.println("Geef een getal in tussen 1 en 45");
                }
                else{
                    userNumbers.add(gok);
                }
            }catch(InputMismatchException ime){
                System.out.println("Gelieve een getal in te geven!");
            }
        }
        ////////////////////////////////////////////////////////////////////////////////

        //now a random lotto solution is generated
        Set<Integer> solutionSet = new HashSet<>();
        while(solutionSet.size()<6){
            solutionSet.add(new Random().nextInt(45)+1);
        }
        System.out.println("De winnende cijfers waren:");
        solutionSet.forEach(System.out::println);
        ////////////////////////////////////////////////////////////////////////////////::

        //now we will see how many numbers (and which numbers) you have guessed correct
        Set<Integer> matchingNumbers= new HashSet<>(solutionSet);
        matchingNumbers.retainAll(userNumbers);

        if (matchingNumbers.size()==0) System.out.println("Sorry, je hebt geen enkel cijfer juist");
        else {
            System.out.println("Je hebt " + matchingNumbers.size() + " juiste cijfers, namelijk: ");
            matchingNumbers.forEach(System.out::println);
        }
    }
}
