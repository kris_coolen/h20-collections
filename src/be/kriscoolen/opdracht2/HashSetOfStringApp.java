package be.kriscoolen.opdracht2;

import java.util.*;

public class HashSetOfStringApp {

    static Scanner keyboard = new Scanner(System.in);
    public static void main(String[] args) {
        //List<Integer> list = new ArrayList<>();
        Set<String> list = new HashSet<>();
        LOOP:
        while (true) {
            System.out.println("geef een woord; Eindig met een punt om te stoppen");
            String word = keyboard.nextLine();
            if (word.endsWith(".")){
                list.add(word.substring(0,word.length()-1));
                break LOOP;
            }
            list.add(word);
        }
        //System.out.println("*** De woorden die je hebt ingegeven in omgekeerde volgorde zijn:***");
        //for (int i = 0; i < list.size(); i++)
        //    System.out.println(list.get(list.size() - 1 - i));

        System.out.println("Het aantal woorden in deze zin: " + list.size());
        System.out.println("hash set afdrukken met een foreach:");
        list.forEach(System.out::println);

    }

}
