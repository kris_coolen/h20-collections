package be.kriscoolen.opdracht6;

import java.util.Objects;

public class Person {
    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;
    private double weight;
    private double height;

    public Person(String firstName, String lastName, Gender gender, int age, double weight, double height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Person\n" +
                "\tfirstName='" + firstName + '\'' +
                "\n\tlastName='" + lastName + '\'' +
                "\n\tgender=" + gender +
                "\n\tage=" + age +
                "\n\tweight=" + weight +
                "\n\theight=" + height +
                "\n**************************";
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName,lastName,gender,age,weight,height);
    }

    @Override
    public boolean equals(Object o) {
        if(o==null) return false;
        if(this==null) return true;
        if(o instanceof Person &&
               getFirstName().equals(((Person)o).getFirstName())&&
               getLastName().equals(((Person)o).getLastName())&&
               getGender().equals(((Person)o).getGender())&&
               getAge()==((Person)o).getAge()&&
               getWeight()==((Person)o).getWeight()&&
              getHeight()==((Person)o).getHeight()
        )
            return true;
        else return false;
    }
}
