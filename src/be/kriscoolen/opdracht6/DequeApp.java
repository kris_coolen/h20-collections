package be.kriscoolen.opdracht6;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeApp {

    public static void main(String[] args) {
        Person p1 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);
        Person p2 = new Person("Peter","Parker",Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse",Gender.F,26,51,1.62);
        Person p4 = new Person("Maggy","Simpson",Gender.F,1,5,0.81);
        Person p5 = new Person("Kent","Clark",Gender.M,28,78,1.83);
        Person p6 = new Person("Daisy","Duck",Gender.F,23,50,1.61);

        Person[] personArray = {p1,p2,p3,p4,p5,p6};
        Deque<Person> row = new ArrayDeque<>();
        System.out.println("creating row of persons: men in front, women at the back of the row");
        for(Person p: personArray){
            if(p.getGender()==Gender.M) row.offerFirst(p);
            else row.offerLast(p);
        }
        System.out.println("generating couples:");
        Person man = row.pollFirst();
        Person woman = row.pollLast();
        int couple = 1;
        while(man!=null&&woman!=null){
            System.out.println("couple " + couple + ":");
            System.out.println(man);
            System.out.println(woman);
            couple++;
            man = row.pollFirst();
            woman=row.pollLast();
        }

    }
}
