package be.kriscoolen.opdracht7;

import java.util.*;

public class PersonApp {
    public static void main(String[] args) {


        Set<Person> personSet = new TreeSet<>();
        Person p1 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);
        Person p2 = new Person("Peter","Parker",Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse",Gender.F,26,51,1.62);
        Person p4 = new Person("Maggy","Simpson",Gender.F,1,5,0.81);
        Person p5 = new Person("Bruce","Wayne",Gender.M,42,82,1.85);

        System.out.println("*****Adding persons to treeSet, sorted by override compareTo function in class Person implements Comparable<Person>: ******");
        personSet.add(p1);
        personSet.add(p2);
        personSet.add(p3);
        personSet.add(p4);
        personSet.add(p5);

        System.out.println("Number of persons in set: " + personSet.size());
        personSet.forEach(System.out::println);

        System.out.println("*****Adding persons to treeSet, sorted by AgeComparator class which implements Comparator<Person>:*****");
        SortedSet<Person> personByAgeSet = new TreeSet<Person>(new AgeComparator());
        personByAgeSet.add(p1);
        personByAgeSet.add(p2);
        personByAgeSet.add(p3);
        personByAgeSet.add(p4);
        personByAgeSet.add(p5);
        personByAgeSet.add(new Person("Another","Mouse",Gender.M,26,51,1.62));

        System.out.println("Number of persons in set: " + personByAgeSet.size());
        personByAgeSet.forEach(System.out::println);

        System.out.println("*****Adding persons to treeSet, sorted by weight using an anonymous nested class  *****");
        SortedSet<Person> personByWeightSet = new TreeSet<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return Double.valueOf(o1.getWeight()).compareTo(Double.valueOf(o2.getWeight()));
            }
        });
        personByWeightSet.add(p1);
        personByWeightSet.add(p2);
        personByWeightSet.add(p3);
        personByWeightSet.add(p4);
        personByWeightSet.add(p5);
        personByWeightSet.add(new Person("Another","Mouse",Gender.M,26,51,1.62));
        System.out.println("Number of persons in set: " + personByWeightSet.size());
        personByWeightSet.forEach(System.out::println);

        System.out.println("*****Adding persons to treeSet (sorted by weight using a lambda expression  *****");
        SortedSet<Person> personByWeightLambdaSet = new TreeSet<>((o1,o2)->Double.valueOf(o1.getWeight()).compareTo(Double.valueOf(o2.getWeight())));
        personByWeightLambdaSet.add(p1);
        personByWeightLambdaSet.add(p2);
        personByWeightLambdaSet.add(p3);
        personByWeightLambdaSet.add(p4);
        personByWeightLambdaSet.add(p5);
        personByWeightLambdaSet.add(new Person("Another","Mouse",Gender.M,26,51,1.62));
        System.out.println("Number of persons in set: " + personByWeightLambdaSet.size());
        personByWeightLambdaSet.forEach(System.out::println);

        System.out.println("*****Adding persons to treeSet (sorted by weight using static method of interface comparator (best practice)  *****");
        SortedSet<Person> personByWeightStaticSet = new TreeSet<>(Comparator.comparingDouble(Person::getWeight));
        personByWeightStaticSet.add(p1);
        personByWeightStaticSet.add(p2);
        personByWeightStaticSet.add(p3);
        personByWeightStaticSet.add(p4);
        personByWeightStaticSet.add(p5);
        personByWeightStaticSet.add(new Person("Another","Mouse",Gender.M,26,51,1.62));
        System.out.println("Number of persons in set: " + personByWeightStaticSet.size());
        personByWeightStaticSet.forEach(System.out::println);


        System.out.println("********Adding persons to treeset now sorted by first age, then (last)name, then weight***********");
        SortedSet<Person> personByAgeNameWeightSet = new TreeSet<>(Comparator
                .comparingInt(Person::getAge)
                .thenComparing(Person::getLastName)
                .thenComparingDouble(Person::getWeight)
        );

        personByAgeNameWeightSet.add(new Person("Hannelore","Coolen",Gender.F,6,25,1.20));
        personByAgeNameWeightSet.add(new Person("Ann-Sophie","Coolen",Gender.F,3,15,1.04));
        personByAgeNameWeightSet.add(new Person("Hanne-lore","Vissers",Gender.F,6,23,1.15));
        personByAgeNameWeightSet.add(new Person("Ella-Marie","Cuzi",Gender.F,6,12,1.02));
        personByAgeNameWeightSet.add(new Person("Dingske","Coolen",Gender.F,3,18,1.10));
        personByAgeNameWeightSet.add(new Person("Hanne-lore","Vissers",Gender.F,6,23,1.15));
        personByAgeNameWeightSet.add(new Person("Hanne-lore","Vissers",Gender.F,6,25,1.2));
        System.out.println("Number of persons in set: " + personByAgeNameWeightSet.size());
        personByAgeNameWeightSet.forEach(System.out::println);


    }
}
