package be.kriscoolen.opdracht1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArrayListOfStringApp {

    static Scanner keyboard = new Scanner(System.in);
    public static void main(String[] args) {
        //List<Integer> list = new ArrayList<>();
        List<String> list = new ArrayList<>();
        LOOP:
        while (true) {
            System.out.println("geef een woord; Eindig met een punt om te stoppen");
            String word = keyboard.nextLine();
            if (word.endsWith(".")){
                list.add(word.substring(0,word.length()-1));
                break LOOP;
            }
            list.add(word);
        }
        System.out.println("*** De woorden die je hebt ingegeven in omgekeerde volgorde zijn:***");
        for (int i = 0; i < list.size(); i++)
            System.out.println(list.get(list.size() - 1 - i));
        System.out.println("Het aantal woorden in deze zin: " + list.size());
        System.out.println("list afdrukken met een foreach:");
        list.forEach(System.out::println);
        System.out.println("list afdrukken als een array");
        String[] listArrayList= list.toArray(new String[0]);
        for(int i=0; i<listArrayList.length;i++){
            System.out.println(listArrayList[i]);
        }

    }
}
