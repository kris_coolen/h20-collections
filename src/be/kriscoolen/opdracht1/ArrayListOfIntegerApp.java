package be.kriscoolen.opdracht1;

import java.util.*;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Scanner;

public class ArrayListOfIntegerApp {

    static Scanner keyboard = new Scanner(System.in);
    public static void main(String[] args) {
        //List<Integer> list = new ArrayList<>();
        List<Integer> list = new LinkedList<>();
        LOOP:
        while(true) {
            System.out.println("geef een getal. Kies 'stop' om te stoppen");
            String intString = keyboard.nextLine();
            if(intString.toLowerCase().equals("stop")) break LOOP;
            try {
                list.add(Integer.parseInt(intString));
                //list.forEach(System.out::println);

            } catch (NumberFormatException nfe) {
                System.out.println("***Gelieve een getal op te geven***");
            }
        }
        System.out.println("*** Je hebt de volgende getallen ingegeven:***");
        list.forEach(System.out::println);
        System.out.println("De som is gelijk aan : " + list.stream().mapToInt(Integer::valueOf).sum());
        System.out.printf("Het gemiddelde is gelijk aan: %.2f",list.stream().mapToInt(Integer::valueOf).average().getAsDouble());

    }

}
