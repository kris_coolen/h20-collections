package be.kriscoolen.opdracht5;

public class BurgerOrder implements Comparable<BurgerOrder> {
    private String name;
    private int number;

    public BurgerOrder(String name, int number){
        this.name = name;
        this.number = number;
    }

    public BurgerOrder(String name){
        this(name,1);
    }

    public BurgerOrder(){
        this("Bicky Burger");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "BurgerOrder{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public int compareTo(BurgerOrder o) {
        return name.compareTo(o.name);
    }
}
