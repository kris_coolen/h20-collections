package be.kriscoolen.opdracht5;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class BurgerOrderApp {

    public static void main(String[] args) {
       // Queue<BurgerOrder> queue = new LinkedList<>();
        Queue<BurgerOrder> queue = new PriorityQueue<>();
        queue.add(new BurgerOrder());
        queue.add(new BurgerOrder("Cheese Burger"));
        queue.add(new BurgerOrder("Vegan Burger",3));
        queue.add(new BurgerOrder("Botanique Classic",10));
        queue.add(new BurgerOrder("Botanique Classic",10));
        //handling queue
        BurgerOrder bo = queue.peek();
        while(bo!=null){
            System.out.println("Making " + bo);
            bo = queue.poll();
            System.out.println(bo + " is ready for service.");
            bo = queue.peek();
        }

    }
}
