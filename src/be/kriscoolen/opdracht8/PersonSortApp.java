package be.kriscoolen.opdracht8;

import java.util.*;

public class PersonSortApp {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        Person p1 = new Person("Bruce","Wayne",Gender.M,42,82.4,1.85);
        Person p2 = new Person("Peter","Parker",Gender.M,21,68,1.72);
        Person p3 = new Person("Minnie","Mouse",Gender.F,26,51,1.62);
        Person p4 = new Person("Maggy","Simpson",Gender.F,1,5,0.81);
        Person p5 = new Person("Bruce","Banner",Gender.M,42,82,1.85);
        list.add(p1);list.add(p2);list.add(p3);list.add(p4);list.add(p5);

        System.out.println("******Sorting by last name using Comparator.naturalOrder() implementen by compareTo function in Person*******");
        list.sort(Comparator.naturalOrder());
        for(Person p:list) System.out.println(p);

        System.out.println("******Sorting by weight using comparator.comparingDouble*******");
        list.sort(Comparator.comparingDouble(Person::getWeight));
        for(Person p: list) System.out.println(p);


    }
}
