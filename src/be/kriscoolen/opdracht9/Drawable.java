package be.kriscoolen.opdracht9;

public interface Drawable extends Scalable {
    public void draw(DrawingContext dc);
}
